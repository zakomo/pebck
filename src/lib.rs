use directories::ProjectDirs;
use std::{fs::File,io::Write,path::PathBuf};
use chrono::prelude::*;
use sqlite::{self, Connection};

pub fn create_dummy_conf_file() {
    let conf_dir = ProjectDirs::from("com", "pebck", "pebck");
    let mut new_file_name = conf_dir.unwrap().config_dir().to_owned();
    new_file_name.push(String::from("pebck.toml"));
    let new_file_content = r#"
    # for a complete syntax of the toml file refer to gitlab or the README file
    # adapt the directories for your case
    # to add more job just add more [[jobs]] sections

    [[jobs]]
    origin = "/home"
    destination = "/backup"
    filename = "backup1"

    "#;

    let mut file = File::create(new_file_name).unwrap();
    writeln!(&mut file, "{}",  new_file_content).unwrap();
}

//gets the current date in YYYYMMDD-hhmmss format.
//TODO: parametrized format.
pub fn current_date(format: Option<String>) -> String { 
    let fmt = format.unwrap_or(String::from("%Y%m%d-%H%M%S"));
    let datelocal = Local::now();
    datelocal.format(&fmt).to_string()
}

pub fn config_file_discovery(filename:Option<String>) -> Option<PathBuf> {
    let filename = filename.unwrap_or(String::from("pebck.toml"));
    if let Some(work_dir) = ProjectDirs::from("com", "pebck", "pebck"){
        let mut fullname = work_dir.config_dir().to_owned();
        fullname.push(filename);
        if fullname.exists() {
            Some(fullname)
        } else {
            None
        }
    } else {
        None
    }
}
/*
pub fn find_or_create_database () -> Option<PathBuf>{
    if let Some(data_dir) = ProjectDirs::from("com", "pebck", "pebck"){
        let mut data = data_dir.data_local_dir().to_owned();
        data.push("pebck.sqlite");
        Some(data)
    }else {
        None
    }
    
}

pub fn initialize_db(db:PathBuf) -> Connection{
    
    let query = r#"
    CREATE TABLE backup if not exists( 
        id numeric primary key,
        filepath text not null,
        creation text,
        data_origin text not null,
        data_stored text not null
    )"#;
    
    let conn = sqlite::open(db).unwrap();
    conn.execute(query).unwrap()
}

pub fn create_id(){
    
}*/