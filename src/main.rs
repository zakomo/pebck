use pebck::{self, current_date};
use std::{path::PathBuf, env};
use std::fs::{File, read_to_string};
use toml::de::from_str;
use serde::Deserialize;
use flate2::{Compression, write::GzEncoder};
use tar::Builder;

#[derive(Deserialize, Debug)]
struct Config{
    overwrite: Option<bool>,
    jobs: Vec<Job>,
}

#[derive(Deserialize, Debug)]
struct Job{
    origin: PathBuf,
    destination: PathBuf,
    filename: String,
}

fn main() {
    // use some trickery to get the file name through command line later on.
    // Probably none of this is required but I am not a brilliant programmer...

    let conf_file_name = String::from("pebck.toml");
    let config = pebck::config_file_discovery(Some(conf_file_name));
    if config.is_some() {
        let conf = load_toml(config.unwrap());
        println!("{:#?}", conf);
        conf.jobs.into_iter().for_each(| job| {
            job.backup(conf.overwrite.unwrap_or(false))        
        });
    } else {
        pebck::create_dummy_conf_file();
    }
}

impl Job{
    fn backup(mut self, overwrite: bool) {
        //build the destination path
        //I should create the destination folder and put the files inside
        let mut fname = self.filename.clone();

        if !overwrite{
            fname = fname + &pebck::current_date(None) + ".tar.gz";
        }

        self.destination.push(fname.clone());
        
        if self.origin.exists(){
            //let query = format!("insert into backup (filepath, creation, data_origin, data_stored) values ({},{},{},{})", fname, current_date(Some(String::from("%+"))), self.origin.to_string_lossy(), self.destination.to_string_lossy());
            //move to the origin folder
            env::set_current_dir(self.origin).unwrap();
            
            //I will create the file straight away and deal with the fallout of a failure later
            //At the moment the program just crashes. 
            let compressed_file = File::create(self.destination).unwrap();
            let mut encoder = GzEncoder::new(compressed_file, Compression::default());

            {
                let mut archive = Builder::new(&mut encoder);
                archive.append_dir_all(".", ".").unwrap();
            }
            
            encoder.finish().unwrap();
        }
    }

}

fn load_toml(filename: PathBuf) -> Config {
    let content = read_to_string(filename).unwrap();
    from_str(&content).unwrap()
}
